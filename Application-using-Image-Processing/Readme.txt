--- Application ---


1. Running binaries with pre-compiled libraries

a) Copy the appropriate zip file in Bin/ to your hard-disk.
b) Unzip the file.
c) Run GUI.exe


2. Building from source

a) Read the report in Documentation/ for installing 3rd party libraries.
b) Create the necessary environment variables and update the path after installing the libraries.
c) Build the source using Visual Studio 2019.

--- Summary ---

The code is divided into several threads. First, there is a separate CameraCapture thread for of the cameras, allowing them all to stream simultaneously. Then, once the images are retrieved from each camera, there are two operations which must be done to create a stitched panorama from the images. The images must be analyzed to find the relationship between them, and the images must be stitched together into one panoramic image.

First, the images have to be analyzed based on the overlap region between the cameras to calculate how much each image needs to be transformed to align the images appropriately. This is done by utilizing some of OpenCV’s computer vision algorithms to calculate a perspective transformation between two images. These parameters are called homographies, so we have dubbed these threads, “Homographiers.” Several of these homographies are calculated between several pairs of images so that in the end, all of the images can be stitched together. Each of these calculations can be done independently, so they are each done in a separate thread.

Once the perspective transformation parameters have been calculated, the main thread can take images from each of the CameraCapture threads, and parameters from each of the Homographier threads, and use them to stitch the images together. The stitching is done on the GPU, since it is an easily parallelizable problem. And the output images are displayed on a GUI built using the Qt framework. This GUI not only displays the stitched video stream, but it also allows the user to make changes to various configuration options on the fly, and see the resulting changes in the output video immediately. These configuration options let the user specify several options about how the images are stitched together, and a plethora of options regarding how the Homographiers should calculate the homographies.

The GUI allows the user to adjust the parameters until they are content with the results, up to the theoretical limits of the algorithms used and the physical limits of the hardware used.
