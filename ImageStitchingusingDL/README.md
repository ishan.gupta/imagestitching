# Deep Image Stitching for ROV

## To run the code

The main code and other codes are located in deep_homography_net folder.

You may run main.py for training and eval.py for evaluation.

### Package requirements:

This project is based on python, pytorch.

- python 3.6+
- numpy, matplotlib
- pytorch, torchvision
- kornia
- (optional) opencv-contrib-python == 3.4.2.17 (Latest version is OK, if not using key points)



### Data:

You may create 'data' folder. Also you should check data folder's location in main.py and eval.py code.

- Sample Dataset: train ,valid (in sample_data folder)

1. Sample imageS from ROV using IMG_generation.py
2. Generate homography dataset using dataset_generation.py