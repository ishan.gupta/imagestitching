#%%
import os

import cv2
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('default')


#%% 
def read_img(img_dir,img_name,return_rgb):
    if return_rgb:
        img = cv2.cvtColor(cv2.imread(os.path.join(img_dir,img_name)),cv2.COLOR_BGR2RGB)
    else:
        img = cv2.imread(os.path.join(img_dir,img_name))
    return img


def stitch_2to1(img1,img2,H_21):
    # Homography matrix can move som pixel location having negative value.
    # Therefore we should check it first.
    crop_size = img1.shape[0:2]
    corners = np.float32([[0,crop_size[1],crop_size[1],0],[0,0,crop_size[0],crop_size[0]],[1,1,1,1]])
    corners_transform = np.matmul(H_21,corners)
    corners_transform_real = np.divide(corners_transform,corners_transform[2,:]).astype('float32')
    bbox = cv2.boundingRect(corners_transform_real[0:2,:].T)
    
    # Now we get bounding box of warped corner.
    # From that information, estimate how much to translate and estimate size of image.
    x_translate = 0 if bbox[0]>0 else -bbox[0]
    y_translate = 0 if bbox[1]>0 else -bbox[1]
    Tr = np.float32([[1,0,x_translate],[0,1,y_translate],[0,0,1]])
    x_size_add = bbox[0] if bbox[0]>0 else 0
    y_size_add = bbox[1] if bbox[1]>0 else 0
    img2_size = (bbox[2]+x_size_add,bbox[3]+y_size_add)
    img1_size = (crop_size[1]+x_translate,crop_size[0]+y_translate)
    new_img_size = (max(img1_size[0],img2_size[0]),\
                    max(img1_size[1],img2_size[1]))
    
    # Now warp the image. Note that we use t*H_21 to remove negative value
    img2_back = cv2.warpPerspective(img2,np.matmul(Tr,H_21),new_img_size)
    img1_back = cv2.warpAffine(img1,Tr[0:2,:],new_img_size)
    
    # Also we want to know alpha channel.
    img2_white = np.ones(img2.shape)
    img1_white = np.ones(img1.shape)
    img2_alpha = cv2.warpPerspective(img2_white,np.matmul(Tr,H_21),new_img_size)
    img1_alpha = cv2.warpAffine(img1_white,Tr[0:2,:],new_img_size)
    
    return img1_back, img2_back, img1_alpha, img2_alpha


def return_overlap(img_A,img_B,alpha_A,alpha_B):
    empty_A = alpha_A==0
    empty_B = alpha_B==0
    img_A_overlap = img_A
    img_A_overlap[empty_B] = 0
    img_B_overlap = img_B
    img_B_overlap[empty_A] = 0
    return img_A_overlap, img_B_overlap


#%%
datadir = './data/train'

imgA = read_img(os.path.join(datadir,'Image_A'),'00000021.png',True)
imgB = read_img(os.path.join(datadir,'Image_B'),'00000021.png',True)

fAB = open(os.path.join(datadir,'H_AB.txt'))
HAB = np.array(fAB.readline().split(',')[2:-1],dtype='float32').reshape((3,3))
fAB.close()

fBA = open(os.path.join(datadir,'H_BA.txt'))
HBA = np.array(fBA.readline().split(',')[2:-1],dtype='float32').reshape((3,3))
fBA.close()

img1_back, img2_back, img1_alpha, img2_alpha = stitch_2to1(imgA,imgB,HBA)
plt.figure()
plt.subplot(121)
plt.imshow(img2_back,alpha=0.5)
plt.imshow(img1_back,alpha=0.5)
plt.subplot(122)
plt.imshow(img2_alpha,alpha=0.5)
plt.imshow(img1_alpha,alpha=0.5)
plt.suptitle('img B to img A')


img1_back, img2_back, img1_alpha, img2_alpha = stitch_2to1(imgB,imgA,HAB)
plt.figure()
plt.subplot(121)
plt.imshow(img2_back,alpha=0.5)
plt.imshow(img1_back,alpha=0.5)
plt.subplot(122)
plt.imshow(img2_alpha,alpha=0.5)
plt.imshow(img1_alpha,alpha=0.5)
plt.suptitle('img A to img B')
