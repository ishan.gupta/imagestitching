#%%
import os
import random

import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
plt.style.use('default')

from progress import bar

PI = np.pi

#%%
'''
######################################################################################
ONLY MODIFY HERE!
'''
MODE = 'generation'
# 'test' : test single image and visualize
# 'generation' : Dataset generation mode

ORIGINAL_DATA_DIR = './data/origin/valid'
OUTPUT_DATA_DIR = './data/OUTPUT/'
# When generation mode, data will be saved here.
# It will autimatically create two folder (A and B), and homography matrix value list.
# The file name will be same as original file name.

SCALE = 1 # between 0 and 1
# This parameter controls image cropping size.
# Setting this parameter 1 makes maximum cropping image size,
# considering the radius of circle and position of image B

DEGREE_OF_OVERLAP = 0.2 # between 0 and 1
# This parameter controls degree of overlap.
# Note that setting this value zero do not means no overlap area. It is relative value.

THETA_VARIANCE = 0 # between 0 and 45, recommend not more than 22.5
# This parameter controls randomness of squareness of image A.
# By setting this parameter 0, it will give square image.
# If this is non zero, the image A will have random size.
# For more detail see figure(will be updated)

PSI_VARIANCE = 15 # between 0 and 90, recommend not more than 22.5
# This parameter controls distortion level of image B.
# By setting this parameter 0, it will give image_B as rectangle.
# For more detail see figure(will be updated)

INPUT_RESIZE = (816,816)
# This parameter constrols resizing of input image.
# Why this is set to 818 is to make output image size 320 X 320 (at SCALE 1 and DEG_OF_OVERLAP 0.2)

'''
######################################################################################
'''

#%%
# Define functions
def read_img(img_dir,img_name,return_rgb):
    if return_rgb:
        img = cv2.cvtColor(cv2.imread(os.path.join(img_dir,img_name)),cv2.COLOR_BGR2RGB)
    else:
        img = cv2.imread(os.path.join(img_dir,img_name))
    return img

def random_homography(img,scale,overlap_amount,theta_var,psi_var,theta_base=45,input_resize=None):
    if overlap_amount>1 or overlap_amount<0:
        raise ValueError('overlap_amount should between 0 and 1')
    
    if input_resize != None:
        img = cv2.resize(img,(input_resize[0],input_resize[1]))
    h,w,_ = img.shape
    alpha = 1-overlap_amount
    r_max = min(h,w)/(2*(1+alpha))
    r = int(r_max*scale)
    x0a = int(random.uniform((1+alpha)*r,w-(1+alpha)*r))
    y0a = int(random.uniform((1+alpha)*r,h-(1+alpha)*r))
    theta = theta_base + random.uniform(-theta_var,theta_var)
    xia = int(x0a-r*np.cos(np.deg2rad(theta)))
    xea = int(x0a+r*np.cos(np.deg2rad(theta)))+1
    yia = int(y0a-r*np.sin(np.deg2rad(theta)))
    yea = int(y0a+r*np.sin(np.deg2rad(theta)))+1
    img_A = img[yia:yea,xia:xea]
    crop_h, crop_w, _ = img_A.shape
    crop_size = np.array([crop_h,crop_w])
    img_A_pt = np.array([[x0a,y0a],[xia,yia],[xea,yea]])

    d = alpha*r
    phi = random.uniform(0,2*PI)
    x0b = int(x0a+d*np.cos(phi))
    y0b = int(y0a+d*np.sin(phi))
    psi1 = random.uniform(0,90)
    x1b = int(x0b+r*np.cos(np.deg2rad(psi1)))
    y1b = int(y0b+r*np.sin(np.deg2rad(psi1)))
    psi2 = psi1 + 90 + random.uniform(-psi_var,psi_var)
    x2b = int(x0b+r*np.cos(np.deg2rad(psi2)))
    y2b = int(y0b+r*np.sin(np.deg2rad(psi2)))
    psi3 = psi2 + 90 + random.uniform(-psi_var,psi_var)
    x3b = int(x0b+r*np.cos(np.deg2rad(psi3)))
    y3b = int(y0b+r*np.sin(np.deg2rad(psi3)))
    psi4 = psi3 + 90 + random.uniform(-psi_var,psi_var)
    x4b = int(x0b+r*np.cos(np.deg2rad(psi4)))
    y4b = int(y0b+r*np.sin(np.deg2rad(psi4)))
    img_B_pt = np.array([[x0b,y0b],[x1b,y1b],[x2b,y2b],[x3b,y3b],[x4b,y4b]])
    
    # transform image
    img_corner = np.float32([[0,0],[0,crop_h-1],[crop_w-1,0],[crop_w-1,crop_h-1]])
    img_A_corner = img_A_pt[1,:].astype('float32')
    H = cv2.getPerspectiveTransform(img_B_pt[[3,2,4,1],:].astype('float32'),img_corner)
    img_B = cv2.warpPerspective(img, H, (crop_h,crop_w))

    # img_B_pt1 = img_B_pt[[3,2,4,1],:].astype('float32')
    # img_B_pt2 = np.float32([[0,0],[0,crop_h-1],[crop_w-1,0],[crop_w-1,crop_h-1]])
    # H = cv2.getPerspectiveTransform(img_B_pt1,img_B_pt2)
    # rel_H = cv2.getPerspectiveTransform(img_B_pt1-img_A_pt[1,:].astype('float32'),img_B_pt2)
    # img_B = cv2.warpPerspective(img, H, (crop_h,crop_w))
    # rel_H_inv = np.linalg.inv(rel_H)
    # rel_H_inv = rel_H_inv/rel_H_inv[2,2]

    # Return image and 4 corner point's delta value    
    # imgae B (2nd input) to image A (1st input) coordinate (this is what originally computed)
    corners_B_in_A = img_B_pt[[3,2,4,1],:].astype('float32') - img_A_corner
    delata_px_BtoA = np.round(corners_B_in_A - img_corner).astype('int')

    # image A (1st input) to image B (2nd input) coordinate
    H_BtoA = cv2.getPerspectiveTransform(corners_B_in_A,img_corner)
    H_BtoA_inv = np.linalg.inv(H_BtoA)
    H_AtoB = H_BtoA_inv/H_BtoA_inv[2,2]
    corners_homogeneous = np.vstack((img_corner.T,[1,1,1,1]))
    corners_A_in_B_homogeneous = np.matmul(H_AtoB,corners_homogeneous)
    corners_A_in_B = corners_A_in_B_homogeneous[0:2,:].T
    delata_px_AtoB = np.round(corners_A_in_B - img_corner).astype('int')

    return H_AtoB, H_BtoA, r, crop_size, img, img_A, img_B, img_A_pt, img_B_pt, delata_px_AtoB, delata_px_BtoA


#%%
# Get list of images in original data directory
original_data_list = os.listdir(ORIGINAL_DATA_DIR)
num = len(original_data_list)-1


#%%
# Run
if MODE == 'test':
    img_idx = random.randint(0,num)
    img_input = read_img(ORIGINAL_DATA_DIR, original_data_list[img_idx], True)
    H_ab, H_ba, r, crop_size, img, img_A, img_B, A_pt, B_pt, deltaAB, deltaBA = random_homography(img_input,
                                                                          SCALE,DEGREE_OF_OVERLAP,
                                                                          THETA_VARIANCE,PSI_VARIANCE,
                                                                          input_resize=INPUT_RESIZE)

    rect_A = patches.Rectangle(A_pt[1,:],crop_size[0],crop_size[1],linewidth=1,edgecolor='r',facecolor='none')
    rect_B = patches.Polygon(B_pt[1:,:],linewidth=1,edgecolor='b',facecolor='none')
    circle_A = patches.Circle(A_pt[0,:], r, linewidth=1,edgecolor='r',facecolor='none')
    circle_B = patches.Circle(B_pt[0,:], r, linewidth=1,edgecolor='b',facecolor='none')
    ax1 = plt.subplot(131)
    ax1.imshow(img)
    ax1.add_patch(rect_A)
    ax1.add_patch(rect_B)
    ax1.add_patch(circle_A)
    ax1.add_patch(circle_B)
    ax2 = plt.subplot(132)
    ax2.imshow(img_A)
    ax3 = plt.subplot(133)
    ax3.imshow(img_B)
    plt.suptitle('image_num #%d, size=(%d,%d)' % (img_idx,crop_size[0],crop_size[1]))
    print(H_ab)
    print(H_ba)
    plt.show()

elif MODE == 'generation':
    Image_A_dir = os.path.join(OUTPUT_DATA_DIR, 'Image_A')
    Image_B_dir = os.path.join(OUTPUT_DATA_DIR, 'Image_B')
    H_AB_file = os.path.join(OUTPUT_DATA_DIR, 'H_AB.txt')
    H_BA_file = os.path.join(OUTPUT_DATA_DIR, 'H_BA.txt')
    Delta_AB_file = os.path.join(OUTPUT_DATA_DIR, 'Delta_AB.txt')
    Delta_BA_file = os.path.join(OUTPUT_DATA_DIR, 'Delta_BA.txt')
    index_name_file = os.path.join(OUTPUT_DATA_DIR, 'index_name.txt')
    config_file = os.path.join(OUTPUT_DATA_DIR, 'config.txt')


    if not os.path.exists(OUTPUT_DATA_DIR):
        os.mkdir(OUTPUT_DATA_DIR)
    if not os.path.exists(Image_A_dir):
        os.mkdir(Image_A_dir)
    if not os.path.exists(Image_B_dir):
        os.mkdir(Image_B_dir)
    if os.path.exists(H_AB_file):
        os.remove(H_AB_file)
    if os.path.exists(H_BA_file):
        os.remove(H_BA_file)
    if os.path.exists(Delta_AB_file):
        os.remove(Delta_AB_file)
    if os.path.exists(Delta_BA_file):
        os.remove(Delta_BA_file)
    if os.path.exists(index_name_file):
        os.remove(index_name_file)        
    if os.path.exists(config_file):
        os.remove(config_file)        


    ## configuration for data generation
    with open(config_file, 'w') as f:
        f.write(f'degree of overlap: {DEGREE_OF_OVERLAP}\n')
        f.write(f'theta variance: {THETA_VARIANCE}\n')
        f.write(f'psi variance: {PSI_VARIANCE}\n')
        
    print('Dataset generation : %s' % ORIGINAL_DATA_DIR)
    f_ab = open(H_AB_file,'w')
    f_ba = open(H_BA_file,'w')
    fd_ab = open(Delta_AB_file,'w')
    fd_ba = open(Delta_BA_file,'w')
    fidxn = open(index_name_file,'w')

    for idx, img_name in enumerate(original_data_list):
        img_input = read_img(ORIGINAL_DATA_DIR, img_name, False)
        H_ab, H_ba, _, _, _, img_A, img_B, _, _, delta_AB, delta_BA = random_homography(img_input,
                                                                    SCALE,DEGREE_OF_OVERLAP,
                                                                    THETA_VARIANCE,PSI_VARIANCE,
                                                                    input_resize=INPUT_RESIZE)
        
        idxn = str(idx).zfill(8)+','+img_name+','
        fidxn.write(idxn)

        value = ''  
        for v in np.reshape(H_ab,(9)) : value += str(v) + ','
        f_ab.write(idxn + value + '\n')

        value = ''  
        for v in np.reshape(H_ba,(9)) : value += str(v) + ','
        f_ba.write(idxn + value + '\n')

        value = ''  
        for v in np.reshape(delta_AB,(8)) : value += str(v) + ','
        fd_ab.write(idxn + value + '\n')
       
        value = ''  
        for v in np.reshape(delta_BA,(8)) : value += str(v) + ','
        fd_ba.write(idxn + value + '\n')

        save_file_name = str(idx).zfill(8)+'.png'
        A_save_path = os.path.join(Image_A_dir,save_file_name)
        B_save_path = os.path.join(Image_B_dir,save_file_name)
        cv2.imwrite(A_save_path,img_A)
        cv2.imwrite(B_save_path,img_B)

        bar(0,idx,num)

    f_ab.close()
    f_ba.close()
    fd_ab.close()
    fd_ba.close()
    fidxn.close()


#%%
