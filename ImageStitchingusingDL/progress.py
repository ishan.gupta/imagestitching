import sys
import time
        
def tic(timer_name=''):
    tn = str(timer_name)
    print('Start %s...' % (tn))
    timer = [tn, time.time()]
    return timer


def toc(tic_name):
    et = time.time()-tic_name[1]
    if tic_name[0] == '':
        print('Elapsed time : %.2f (sec)\n' % (et))
    else:
        print('%s, elapsed time : %.2f (sec)\n' % (tic_name[0], et))
    return et


def bar(initial,count,end):
    iteration = count - initial
    total = end - initial
    bar_len = 40
    filled_len = int(round(bar_len * iteration / float(total)))
    percents = round(100.0 * iteration / float(total), 1)
    bar = '#' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('\r')
    sys.stdout.write('[%s] %s%s' % (bar, percents, '%'))
    sys.stdout.flush()
    
    if iteration == total:
        sys.stdout.write('\n')
    
    
def text(initial,count,end):
    iteration = count - initial
    total = end - initial
    
    sys.stdout.write('\r')
    sys.stdout.write('Iteration : %d / %d' % (iteration,total))
    sys.stdout.flush()
    
    if iteration == total:
        sys.stdout.write('\n')