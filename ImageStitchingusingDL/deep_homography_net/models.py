import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models
import kornia


###
class ConvBnAct(nn.Module):
    def __init__(self, input_dim, output_dim, kerenl_size, stride):
        super(ConvBnAct, self).__init__()
        padding_size = kerenl_size // 2
        self.conv = nn.Conv2d(input_dim, output_dim, kerenl_size, stride=stride, padding=padding_size)
        self.bn = nn.BatchNorm2d(output_dim)
        self.act_fn = nn.LeakyReLU()

    def forward(self,x):
        x = self.act_fn(self.bn(self.conv(x)))
        return x


class BnActConv(nn.Module):
    def __init__(self, input_dim, output_dim, kerenl_size, stride):
        super(BnActConv, self).__init__()
        padding_size = kerenl_size // 2
        self.bn = nn.BatchNorm2d(input_dim)
        self.act_fn = nn.LeakyReLU()
        self.conv = nn.Conv2d(input_dim, output_dim, kerenl_size, stride=stride, padding=padding_size)

    def forward(self,x):
        x = self.conv(self.act_fn(self.bn(x)))
        return x


# Type1, VGG ########################################
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.features = nn.Sequential(
            ConvBnAct( 6, 32, 3, 1 ),
            nn.MaxPool2d(2),
            ConvBnAct( 32, 32, 3, 1 ),
            nn.MaxPool2d(2),
            ConvBnAct( 32, 64, 3, 1 ),
            nn.MaxPool2d(2)
        )
        self.estimator = nn.Sequential(
            ConvBnAct( 64, 64, 3, 2 ),
            ConvBnAct( 64, 64, 3, 1 ),
            ConvBnAct( 64, 128, 3, 2 ),
            ConvBnAct( 128, 128, 3, 1 ),
            ConvBnAct( 128, 256, 3, 2 ),
            ConvBnAct( 256, 256, 3, 1 ),
            nn.AvgPool2d(5)
        )
        self.fc = nn.Linear(256,8)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data,a=0.01)
                m.bias.data.fill_(0)
            if isinstance(m, nn.Linear):
                m.weight.data.fill_(0)
                m.bias.data.fill_(0)
        
    def forward(self, img_fix, img_move):
        x = torch.cat((img_fix,img_move),dim=1)
        x = self.features(x)
        x = self.estimator(x)
        x = x.view(-1,256)
        x = self.fc(x)
        return x.clamp_(-2.,2.)


# Type2, VGG #######################################
class Net2(nn.Module):
    def __init__(self):
        super(Net2, self).__init__()
        self.features = nn.Sequential(
            ConvBnAct( 3, 16, 3, 1 ),
            nn.MaxPool2d(2),
            ConvBnAct( 16, 16, 3, 1 ),
            nn.MaxPool2d(2),
            ConvBnAct( 16, 32, 3, 1 ),
            nn.MaxPool2d(2)
        )
        self.estimator = nn.Sequential(
            ConvBnAct( 64, 64, 3, 2 ),
            ConvBnAct( 64, 64, 3, 1 ),
            ConvBnAct( 64, 128, 3, 2 ),
            ConvBnAct( 128, 128, 3, 1 ),
            ConvBnAct( 128, 256, 3, 2 ),
            ConvBnAct( 256, 256, 3, 1 ),
            nn.AvgPool2d(5)
        )
        self.fc = nn.Linear(256,8)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data,a=0.01)
                m.bias.data.fill_(0)
            if isinstance(m, nn.Linear):
                m.weight.data.fill_(0)
                m.bias.data.fill_(0)

    def forward(self, img_fix, img_move):
        f1 = self.features(img_fix)
        f2 = self.features(img_move)
        x = torch.cat((f1,f2),dim=1)
        x = self.estimator(x)
        x = x.view(-1,256)
        x = self.fc(x)
        return x.clamp_(-2.,2.)


# Type2, Resnet #######################################
class ResidualBlock(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(ResidualBlock, self).__init__()
        self.downsample = (input_dim != output_dim)
        if self.downsample:
            stride = 2
            self.reduce_dim = nn.Conv2d(input_dim, output_dim, 1, stride, 0)
        else:
            stride = 1
        self.layer = nn.Sequential(
            BnActConv(input_dim,  output_dim, 3, stride),
            BnActConv(output_dim, output_dim, 3, 1)
        )
    
    def forward(self,x):
        if self.downsample:
            out = self.reduce_dim(x) + self.layer(x)
        else:
            out = x + self.layer(x)
        return out


class Net3(nn.Module):
    def __init__(self):
        super(Net3, self).__init__()
        self.features = nn.Sequential(
            ConvBnAct( 3, 16, 3, 1 ),
            nn.MaxPool2d(2),
            ConvBnAct( 16, 16, 3, 1 ),
            nn.MaxPool2d(2),
            ConvBnAct( 16, 32, 3, 1 ),
            nn.MaxPool2d(2)
        )
        self.estimator = nn.Sequential(
            ResidualBlock(64, 64),
            ResidualBlock(64, 64),
            ResidualBlock(64, 128),
            ResidualBlock(128, 128),
            ResidualBlock(128, 256),
            ResidualBlock(256, 256),
            nn.AvgPool2d(10)
        )
        self.fc = nn.Linear(256,8)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data,a=0.01)
                m.bias.data.fill_(0)
            if isinstance(m, nn.Linear):
                m.weight.data.fill_(0)
                m.bias.data.fill_(0)

    def forward(self, img_fix, img_move):
        f1 = self.features(img_fix)
        f2 = self.features(img_move)
        x = torch.cat((f1,f2),dim=1)
        x = self.estimator(x)
        x = x.view(-1,256)
        x = self.fc(x)
        return x.clamp_(-2.,2.)

# DLT v2 #########################################################
def generate_A(pts1,pts2):
    batch, n, _ = pts1.shape
    dev = pts1.device
    pts1 = pts1.reshape(-1,8,1)
    pts2 = pts2.reshape(-1,8,1)
    
    W = torch.zeros((batch,18*4,8)).to(dev)
    for i in range(4):
        W[:, 18*i+6:18*i+8,     2*i+0] = pts1[:, 2*i+0:2*i+2,   0]
        W[:, 18*i+8,            2*i+0] = torch.ones((batch))
        W[:, 18*i+15:18*i+17,   2*i+1] = pts1[:, 2*i+0:2*i+2,   0]
        W[:, 18*i+17,           2*i+1] = torch.ones((batch))
    
    b = torch.zeros((batch,18*4,1)).to(dev)
    for i in  range(4):
        b[:, 18*i+0:18*i+2,     0] = -pts1[:, 2*i+0:2*i+2,  0]
        b[:, 18*i+2,            0] = -torch.ones((batch))
        b[:, 18*i+12:18*i+14,   0] = -pts1[:, 2*i+0:2*i+2,  0]
        b[:, 18*i+14,           0] = -torch.ones((batch))

    a = torch.einsum('bij,bjk->bik',W,pts2)+b
    A = a.reshape(-1,8,9)
    
    return A

def DLT_v2(delta):
    batch = delta.shape[0]
    dev = delta.device
    delta = delta.view((-1,4,2))

    corner = torch.FloatTensor([[-1,-1],[-1,1],[1,-1],[1,1]]).unsqueeze(0)
    pts1 = torch.cat([corner]*batch, dim=0).to(dev)
    pts2 = pts1 + delta

    A = generate_A(pts1,pts2)

    # If we only use svd, since lase singular value is close to zero, the backpropagation do not occurs.
    # Therefore, we use svd to compute h33,
    # And multiply it to inhomogeneous solution 
    
    # Compute h33
    try:
        _ ,_, V = torch.svd(A.detach(),some=False) # Not compute gradient of svd
    except:
        print(A)
        raise RuntimeError
    h33 = V[:,8,8]
    
    # Compute Inhomogeneous h
    B = A[:,:,:8] # (B*8*8)
    b = -A[:,:,8].unsqueeze(2) # (B*8*1)
    h_temp, _ = torch.solve(b,B) # h_temp : (B*8*1)
    h_inhomogen = torch.ones((batch,9)).to(dev)
    h_inhomogen[:,:8] = h_temp.squeeze(2)

    # Multiply h33
    h_inhomogen = torch.einsum('bi,b->bi',h_inhomogen,h33)
    
    H_norm = h_inhomogen.reshape(-1,3,3)
    return H_norm



# Photometric l1 loss ##########################################
def photometric_loss(H_norm,img_fix,img_move):
    b, c, w, h = img_fix.shape
    dev = img_fix.device

    # Since we assumed corner point coordinate is -1~1, Normalization will be given as follows:
    # N = torch.FloatTensor([[2/(w-1),0,-1],[0,2/(h-1),-1],[0,0,1]]).unsqueeze(0)
    # N_inv = torch.inverse(N)
    # # To compensate the negative movement,we should translate image.
    # # T = torch.FloatTensor([[1,0,w],[0,1,h],[0,0,1]]).unsqueeze(0)

    # # Matrix should be have same batch size
    # N = torch.cat([N]*b).to(dev)
    # N_inv = torch.cat([N_inv]*b).to(dev)
    # # T = torch.cat([T]*b).to(dev)

    # # Denormalize
    # H = torch.einsum('bij,bjk->bik',N_inv,torch.einsum('bij,bjk->bik',H_norm,N))

    # # Compensate the translation
    # # H = torch.einsum('bij,bjk->bik',T,H)

    # # fixed image has only translation
    # # img_fix_transformed = kornia.warp_perspective(img_fix,T,(h*3,w*3))
    # img_fix_transformed = img_fix

    # # warp the second image
    # # img_move_transformed = kornia.warp_perspective(img_move,H,(h*3,w*3))
    # img_move_transformed = kornia.warp_perspective(img_move,H,(h,w))
    # # img_move_transformed[img_move_transformed != img_move_transformed]=0

    warper = kornia.HomographyWarper(h,w)
    img_move_transformed = warper(img_move,torch.inverse(H_norm))
    img_fix_transformed = img_fix
    
    # Compute L1 loss
    loss = F.l1_loss(img_move_transformed,img_fix_transformed,reduction='sum')

    return loss/(2*h*w), img_move_transformed


if __name__ == '__main__':
    x = np.random.randn(3,6,320,320).astype(np.float32)
    x = torch.from_numpy(x)
    net = Net()
