import sys
import time
import os
import argparse
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.utils as utils
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
import kornia
import matplotlib.pyplot as plt

from models import Net, Net2, Net3
from pipeline import HomographyDataset
from models import DLT_v2, photometric_loss


def main(args):
    MAX_EPOCH = args.max_epoch
    BATCH_SIZE = args.batch_size
    LEARNING_RATE = args.lr
    LRSTEP = args.lr_step
    GAMMA = args.lr_gamma
    L2_DECAY = args.l2_decay
    USE_TENSORBOARD = args.use_tensorboard
    weight = 10


    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print('Using device: %s' % device)

    # Data ####################################################################
    dataset_train = HomographyDataset('../data/train_warp')
    dataset_valid = HomographyDataset('../data/valid_warp')

    dataloader_train = DataLoader(dataset_train,batch_size=BATCH_SIZE,shuffle=True,num_workers=4)
    dataloader_valid = DataLoader(dataset_valid,batch_size=BATCH_SIZE,shuffle=True,num_workers=4)

    # Initialize network ######################################################
    model = Net3()
    model = torch.nn.DataParallel(model)
    model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=LEARNING_RATE, weight_decay=L2_DECAY)
    scheduler = optim.lr_scheduler.StepLR(optimizer,step_size=LRSTEP, gamma=GAMMA)

    # Training ################################################################
    if USE_TENSORBOARD: summary = SummaryWriter()
    it = 0
    best_validation_loss = np.inf
    train_losses = []
    validation_losses = []
    ckpt_dir = 'training_results'
    
    # check path
    if not os.path.exists(ckpt_dir):
        os.mkdir(ckpt_dir)

    # save the training configuration
    train_config_path = os.path.join(ckpt_dir, 'train_config.txt')
    with open(train_config_path,'w') as f:
        f.write('%d\n%d\n%.8f\n%d\n%.8f\n%.12f'%(
            MAX_EPOCH,BATCH_SIZE,LEARNING_RATE,LRSTEP,GAMMA,L2_DECAY))

    # load the model parameters
    ckpt_path = os.path.join(ckpt_dir, 'lastest.pt')
    if os.path.exists(ckpt_path):
        ckpt = torch.load(ckpt_path)
        try:
            model.load_state_dict(ckpt['model'])
            optimizer.load_state_dict(ckpt['optimizer'])
            best_validation_loss = ckpt['best_validation_loss']  
            print('checkpoint is loaded (validation loss:%.4f)' % best_validation_loss)
        except RuntimeError as e:
            print('wrong checkpoint')


    for epoch in range(MAX_EPOCH):
        one_epoch_start = time.time()

        # train
        model.train()
        for data in dataloader_train:
            it += 1
            img1, img2 = data['image1'].to(device), data['image2'].to(device)
            delta_true = data['label_2to1'].to(device)
            delta = model(img1,img2)
            # Supervised
            loss_supervised = F.mse_loss(delta,delta_true)
            # Unsupervised
            loss_unsupervised, img2_at1 = photometric_loss(DLT_v2(delta),img1,img2)
            _, img2_at1_true = photometric_loss(DLT_v2(delta_true),img1,img2)
            # total
            loss = weight*loss_supervised + loss_unsupervised

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            if it % 10 == 0 :
                print('Epoch:{}, Iter:{}, TrainLoss:{:.4f}'.format(epoch, it, loss))
                train_losses.append(loss)
                if USE_TENSORBOARD:
                    summary.add_scalar('train_loss/all', loss, it)
                    summary.add_scalar('train_loss/supervised', weight*loss_supervised.item(), it)
                    summary.add_scalar('train_loss/unsupervised', loss_unsupervised.item(), it)
                    if it % 100 == 0:
                        summary.add_image('Ture', img1*0.5 + img2_at1_true*0.5, dataformats='NCHW')
                        summary.add_image('Estimated', img2_at1, dataformats='NCHW')
                        for name, param in model.module.named_parameters():
                            if not('bn' in name): 
                                name = ('/').join(name.split('.'))
                                summary.add_histogram(name, param, it)
                    summary.flush()
        scheduler.step()

        # validation
        with torch.no_grad():
            model.eval()
            validation_loss = 0.
            validation_loss_supervised = 0.
            validation_loss_unsupervised = 0.
            for data in dataloader_valid:
                img1, img2 = data['image1'].to(device), data['image2'].to(device)
                delta_true = data['label_2to1'].to(device)
                delta = model(img1,img2)
                # Supervised
                loss_supervised = F.mse_loss(delta,delta_true)
                # Unsupervised
                loss_unsupervised, _ = photometric_loss(DLT_v2(delta),img1,img2)
                # total
                loss = weight*loss_supervised + loss_unsupervised
                validation_loss += loss
                validation_loss_supervised += loss_supervised
                validation_loss_unsupervised += loss_unsupervised
            validation_loss /= len(dataloader_valid)
            validation_loss_supervised /= len(dataloader_valid)
            validation_loss_unsupervised /= len(dataloader_valid)
            validation_losses.append(validation_loss)
            if USE_TENSORBOARD:
                summary.add_scalar('validation_loss/all',validation_loss, it)
                summary.add_scalar('validation_loss/supervised',weight*validation_loss_supervised.item(), it)
                summary.add_scalar('validation_loss/unsupervised',validation_loss_unsupervised, it)
                summary.flush()
        
        saved = ''
        if validation_loss < best_validation_loss:
            best_validation_loss = validation_loss
            ckpt = {'model': model.state_dict(),
                    'optimizer': optimizer.state_dict(),
                    'best_validation_loss': best_validation_loss}
            torch.save(ckpt, ckpt_path)
            saved = 'best_loss'
        
        one_epoch_elapsed = time.time()-one_epoch_start
        print('Epoch:{}, Iter:{}, ValidLoss:{:.4f}, time: {:.2f}s {}'.format(epoch, it, validation_loss,one_epoch_elapsed, saved))

    if USE_TENSORBOARD: summary.close()

    with open('training_results/train_losses.txt','w') as f:
        for item in train_losses:
            f.write('%.8f\n' % item)
    with open('training_results/validation_losses.txt','w') as f:
        for item in validation_losses:
            f.write('%.8f\n' % item)


#%%
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--max_epoch', type=int, default=10)
    parser.add_argument('--batch_size', type=int, default=30)
    parser.add_argument('--lr', type=float, default=0.0005)
    parser.add_argument('--lr_step', type=int, default=10)
    parser.add_argument('--lr_gamma', type=float, default=0.1)
    parser.add_argument('--l2_decay', type=float, default=0)
    parser.add_argument('--use_tensorboard', type=bool, default=False)
    args = parser.parse_args(sys.argv[1:])
    main(args) 
