import argparse
import sys
import random
import os
import numpy as np
import matplotlib.pyplot as plt
import cv2
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import kornia

from models import Net, Net2, Net3
from pipeline import HomographyDataset
from models import DLT_v2, photometric_loss


def visualize(img1, img2, title=''):
    plt.imshow(img1,alpha=0.5)
    plt.imshow(img2,alpha=0.5)
    plt.title(title)


def evaluate(args):
    RESULT_DIR = args.result_dir
    MODE = args.mode
    INDEX = args.index
    NUM = args.net_num
    
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print('Using device: %s' % device)

    dataset_test = HomographyDataset('../data/valid')
    dataloader_test = DataLoader(dataset_test,batch_size=30,shuffle=False,num_workers=4)

    Networks = [Net, Net2, Net3]
    model = Networks[NUM-1]()
    model = torch.nn.DataParallel(model)
    model.to(device)

    ckpt_dir = RESULT_DIR
    ckpt_path = os.path.join(ckpt_dir, 'lastest.pt')
    if os.path.exists(ckpt_path):
        ckpt = torch.load(ckpt_path)
        try:
            model.load_state_dict(ckpt['model'])
            best_loss = ckpt['best_validation_loss']  
            print('checkpoint is loaded (validation loss:%.4f)' % best_loss)
        except RuntimeError as e:
            print('wrong checkpoint')
    else:
        raise(RuntimeError)
    
    with torch.no_grad():
        model.eval()
        test_loss = 0.

        if MODE == 'single':
            if INDEX == 'random':
                INDEX = random.randint(0,len(dataset_test))
            else:
                INDEX = int(INDEX)
            print('Testing data Num = %d'%INDEX)
            data = dataset_test[INDEX]
            img1, img2 = data['image1'].to(device).unsqueeze(0), data['image2'].to(device).unsqueeze(0)
            delta_true = data['label_2to1'].to(device).unsqueeze(0)
            delta_est = model(img1,img2)

            # Supervised loss
            loss_supervised = F.mse_loss(delta_est,delta_true)
            print(loss_supervised)

            # Visulaize
            img1_np = img1.squeeze(0).permute((1,2,0)).cpu().numpy()
            img2_np = img2.squeeze(0).permute((1,2,0)).cpu().numpy()

            Hnorm_true = DLT_v2(delta_true)
            _, img2_at1_true = photometric_loss(Hnorm_true,img1,img2)

            Hnorm_est = DLT_v2(delta_est)
            _, img2_at1_est = photometric_loss(Hnorm_est,img1,img2)

            plt.figure()
            plt.subplot(221)
            plt.imshow(img1_np)
            plt.title('Image1')
            plt.subplot(222)
            plt.imshow(img2_np)
            plt.title('Image2')
            plt.subplot(223)
            visualize(img1_np, kornia.tensor_to_image(img2_at1_true), title='True')
            plt.subplot(224)
            visualize(img1_np, kornia.tensor_to_image(img2_at1_est), title='Estimated')
            plt.show()
            plt.savefig('Test_Result.png', dpi=300)
            plt.close('all')        

        elif MODE == 'all':
            validation_loss_supervised = 0.
            for data in dataloader_test:
                img1, img2 = data['image1'].to(device), data['image2'].to(device)
                delta_true = data['label_2to1'].to(device)
                delta_est = model(img1,img2)

                loss_supervised = F.mse_loss(delta_est,delta_true)
                validation_loss_supervised += loss_supervised.item()
            validation_loss_supervised /= len(dataloader_test)
            print('Average Loss = {:.4f}'.format(validation_loss_supervised))

        else:
            print('Check Mode name. No input mode exists.')



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--result_dir', default='training_results', type=str)
    parser.add_argument('--mode', default='single', type=str)
    parser.add_argument('--index', default='random', type=str)
    parser.add_argument('--net_num', default=1, type=int)
    args = parser.parse_args(sys.argv[1:])
    evaluate(args)