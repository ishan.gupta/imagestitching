#%%
import struct
import os
import urllib
import zipfile
import pickle
import PIL
import numpy as np
import matplotlib.pyplot as plt
import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import torchvision.transforms.functional as TF

#%%
def img_reader(filename):
    img = PIL.Image.open(filename)
    w, h = img.size
    return np.asarray(img.resize((320,320))), w, h


class Normalize:
    def __call__(self, sample):
        image1, image2, label_2to1, label_1to2, scale = sample['image1'], sample['image2'], sample['label_2to1'], sample['label_1to2'], sample['scale']
        image1 = image1.transpose((2,0,1))/255
        image2 = image2.transpose((2,0,1))/255
        image1 = torch.from_numpy(image1)
        image2 = torch.from_numpy(image2)
        label_2to1 = label_2to1*scale/160
        label_1to2 = label_1to2*scale/160

        return {'image1': image1,
                'image2': image2,
                'label_2to1': torch.from_numpy(label_2to1),
                'label_1to2': torch.from_numpy(label_1to2)}

class HomographyDataset(Dataset):
    def __init__(self,data_dir):
        self.data_dir = data_dir
        self.transforms = transforms.Compose([Normalize()])

        files_in_dir = os.listdir(self.data_dir)
        delta_ab_file_name = [fname for fname in files_in_dir if 'Delta_AB' in fname][0]
        delta_ba_file_name = [fname for fname in files_in_dir if 'Delta_BA' in fname][0]

        self.delta_ba = np.genfromtxt(os.path.join(self.data_dir,delta_ba_file_name),delimiter=',')
        self.delta_ab = np.genfromtxt(os.path.join(self.data_dir,delta_ab_file_name),delimiter=',')

    def __len__(self):
        return self.delta_ab.shape[0]
    
    def __getitem__(self, idx):

        img1_path = os.path.join(self.data_dir,'Image_A/'+str(idx).zfill(8)+'.png')
        img2_path = os.path.join(self.data_dir,'Image_B/'+str(idx).zfill(8)+'.png')
        img1, w1, h1 = img_reader(img1_path)
        img2, w2, h2 = img_reader(img2_path)
        label_2to1 = self.delta_ba[idx,2:10].astype(np.float32)
        label_1to2 = self.delta_ab[idx,2:10].astype(np.float32)

        sample = {'image1': img1.astype(np.float32), 'image2': img2.astype(np.float32),
                  'label_2to1': label_2to1, 'label_1to2': label_1to2,
                  'scale': float(320/w1)}
        sample = self.transforms(sample)
        return sample


if __name__ == '__main__':
    dataset = HomographyDataset('../data/train')
    dataloader = DataLoader(
        dataset,
        batch_size=4,
        shuffle=True,
        num_workers=0)

    for i_batch, sample_batch in enumerate(dataloader):
        print(sample_batch['image1'].size())
        print(sample_batch['image2'].size())
        print(sample_batch['label_2to1'].size())
        print(sample_batch['label_1to2'].size())
