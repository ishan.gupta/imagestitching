from PIL import Image
import requests
from io import BytesIO
from IPython.display import display
import numpy as np


def get_image(gap_x=0, gap_y=0) :
    zoom = 18
    gap = zoom - 6
    x_min = 53 * pow(2, gap)
    x_max = 55 * pow(2, gap) + (2*gap - 1)
    y_min = 22 * pow(2, gap)
    y_max = 26 * pow(2, gap) + (2*gap - 1)

    x = int(x_min + gap_x)
    y = int(y_min + gap_y)

    url = f'http://serverfeed.jpeg'

    #img = np.zeros((256, 256,3), np.uint8)
    img = np.array([])

    try :
        response = requests.get(url)
        img = Image.open(BytesIO(response.content))
    except:
        print('error')

    return np.array(img)


def merge_image(x, y, num_tile=3) :
    num = num_tile
    output = np.zeros((256*num, 256*num,3), np.uint8)
    
    for i in range(num) :
        for j in range(num) :
            img = get_image(x+j,y+i) 
            if img.size > 0 :
                output[256*i:256*i+256, 256*j:256*j+256] = img                
            else  :
                return np.array([])

    return output


def run(sx, sy, ex, ey):
    num = 3
    output = np.zeros((256*num, 256*num,3), np.uint8)

    for x in range(sx, ex):
        for y in range(sy, ey):
            output = merge_image(x,y)
            if output.size > 0:
                output = Image.fromarray(output, 'RGB')
                output.save(f"im_{x}_{y}.png")
                print(f"OK  : im_{x}_{y}.png ")
            else:
                print(f"FAIL: im_{x}_{y}.png ")


if __name__ == '__main__' :
dat=[] #from server
    for i in range(len(dat)):
        run(dat[i][0][0], dat[i][0][1], dat[i][1][0], dat[i][1][1])
